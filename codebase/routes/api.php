<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('orders', 'OrderController@newOrder');
Route::patch('orders/{id}', 'OrderController@acceptOrder')->where('id', '[1-9][0-9]*');

Route::get('orders', 'OrderController@listOrders');
