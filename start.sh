#!/usr/bin/env bash

red=$'\e[1;31m'
grn=$'\e[1;32m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
white=$'\e[0m'

#sudo apt update
#sudo apt install -y curl

echo " $red ----- Installing Pre requisites ------- $white "
docker-compose down && docker-compose up --build -d

echo " $grn -------Installing Dependencies -----------$blu "
docker-compose run composer install  --ignore-platform-reqs --no-interaction --no-progress --quiet

echo " $red ----- Running Migrations & Data Seeding ------- $white "
chmod 777 -R ./codebase/*
docker exec order_apis_php php artisan migrate
docker exec order_apis_php php artisan db:seed

echo " $red ----- Running Unit test cases ------- $white "
docker exec order_apis_php php ./vendor/phpunit/phpunit/phpunit /var/www/html/tests/Unit

echo " $red ----- Running Intergration test cases ------- $white "
docker exec order_apis_php php ./vendor/phpunit/phpunit/phpunit /var/www/html/tests/Feature

exit 0